package com.itb.calendar.presentation.screens.dayofweek;

import androidx.lifecycle.ViewModel;

import com.itb.calendar.domain.Mp;
import com.itb.calendar.repository.MpRepository;
import com.itb.calendar.repository.MpRepositoryFactory;

public class MpDetailFragmentViewModel extends ViewModel {

    private Mp mp;

    public void loadModule(int mpNumber){
        MpRepository mpRepository = MpRepositoryFactory.getInstance();
        mp = mpRepository.getModuleByNumber(mpNumber);
    }

    public Mp getMp() {
        return mp;
    }
}
