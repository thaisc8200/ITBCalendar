package com.itb.calendar.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import com.itb.calendar.R;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

    // Afegir si es vol mostrar el 'back' a la actionBar
        NavigationUI.setupActionBarWithNavController(this,findNavController(this, R.id.nav_host_fragment));
    }
    @Override
    public boolean onSupportNavigateUp() {
        // Afegir si es vol mostrar el 'back' a la actionBar
        return findNavController(this, R.id.nav_host_fragment).navigateUp();
    }

}
