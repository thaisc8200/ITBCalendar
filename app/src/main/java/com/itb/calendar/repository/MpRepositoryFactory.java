package com.itb.calendar.repository;

import com.itb.calendar.repository.data.MpData;
import com.itb.calendar.repository.data.SessionData;

/**
 * MpRepository Factory
 */
public class MpRepositoryFactory {
    private static MpRepository mpRepository;

    /**
     * @return singleton instance of MpRepository
     */
    public static MpRepository getInstance() {
        if(mpRepository==null){
            mpRepository = new MpRepository(new SessionData(new MpData()));
        }
        return mpRepository;
    }
}
