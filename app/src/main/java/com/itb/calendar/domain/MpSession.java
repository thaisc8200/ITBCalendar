package com.itb.calendar.domain;

public class MpSession {
    Mp mp;
    String startTime;
    String endTime;

    public MpSession(Mp mp, String startTime, String endTime) {
        this.mp = mp;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Mp getMp() {
        return mp;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
